\section{Newton's Method}
\begin{sectionbox}[Newton's Method]
    \paragraph{1-dimensional Newton's Method}

    \[\begin{aligned}
        x_{t+1} := x_t - \frac{f\left( x_t \right)}{f'\left( x_t \right)}
    \end{aligned}\]

    The Babylonian method for finding roots is just the above, applied to $f\left( x \right) = x^{2} - R$.

    \paragraph{Newton's Method for optimization}

    We can apply Newton's method to find minima of a twice differentiable function $f$:
    \[\begin{aligned}
        x_{t+1} &:= x_t - \nabla ^{2} f\left( x_t \right)^{-1} \nabla f\left( x_t \right)
    \end{aligned}\]

    We can interpret the above as a kind of adaptive gradient descent, where we multiply the gradient by a matrix $H\left( x \right)$.

    \paragraph{Newton's Method is affine invariant}

    Newton's Method is affine invariant under any invertible affine transformation.

    Let $f : \mathbb{R} ^{d} \to \mathbb{R} $ be twice differentiable, $A \in \mathbb{R} ^{d\times d}$ an invertible matrix, $b \in \mathbb{R} ^{d}$. Let $g : \mathbb{R} ^{d} \to \mathbb{R} ^{d}$ be the affine function $g\left( y \right) = Ay + b$. Finally, for a twice differentiable function $h$ let $N_h$ be the Newton step for $h$, i.e.
    \[\begin{aligned}
        N_h := x - \nabla ^{2} h\left( x \right)^{-1} \nabla h\left( x \right).
    \end{aligned}\] Then we have $N_{f \circ g} = g^{-1} \circ N_f \circ g$.
\end{sectionbox}

\begin{sectionbox}[]
    Let $f$ be convex and twice differentiable at $x_t \in \text{dom} \left( f \right) $ with $\nabla ^{2}f\left( x_t \right) \succ 0$ being invertible. The vector $x_{t+1}$ resulting from the Newton step satisfies
       $ x_{t+1} = \arg \min _{x \in \mathbb{R} ^{d}} f\left( x_t \right) + \nabla f\left( x_t \right)^{T} \left( x - x_t \right) + \frac{1}{2} \left( x - x_t \right)^{T} \nabla ^{2}f\left( x_t \right) \left( x - x_t \right).$
\end{sectionbox}



% \begin{color}{gray}
%     TODO: Local convergence results for Newton's method?
% \end{color}


\section{Quasi-Newton Methods}

Newton's method is computationally expensive, because we need to compute and invert a Hessian.

\begin{sectionbox}[Quasi-Newton Methods]
    For a differentiable $f :\mathbb{R} ^{d}\to \mathbb{R} $, Quasi-Newton methods use an update step $ x_{t+1} &= x_t - H_t^{-1} \nabla f\left( x_t \right)$ where $H_t \in \mathbb{R} ^{d \times d}$ is a symmetric matrix satisfying the $d$-dimensional secant condition
    \[\begin{aligned}
        \nabla f\left( x_t \right) - \nabla f\left( x_{t-1} \right) = H_t \left( x_t - x_{t-1} \right).
    \end{aligned}\]

    In the 1-dimensional case, the only method which satisfies it is the secant method. In the $d$-dimensional case, it is undetermined.

    Newton's method is a Quasi-Newton method only in specific cases! (when $f$ is a non-degenerate quadratic function)
\end{sectionbox}

\paragraph{Greendstadt family}

Let $M \in \mathbb{R} ^{d \times d}$ be a symmetric and invertible matrix. Consider the Quasi-Newton method
    $x_{t+1} = x_t - H_t^{-1} \nabla f\left( x_t \right)$
where $H_0 = I$ (or some other positive definite matrix), and $H_t^{-1} = H_{t-1}^{-1} + E_t$ is chosen for all $t\ge 1$ in such a way that $H_t^{-1}$ is symmetric and satisfies the secant condition
\[\begin{aligned}
    \nabla f\left( x_t \right) - \nabla f\left( x_{t-1} \right) = H_t\left( x_t - x_{t-1} \right).
\end{aligned}\] For any fixed $t$, set
\[\begin{aligned}
    H &:= H_{t-1}^{-1}\\
    H' &:= H_t^{-1},
    \sigma  &:= x_t - x_{t-1}\\
    y &:= \nabla f\left( x_t \right) - \nabla f\left( x_{t-1} \right)
\end{aligned}\] and define
\[\begin{aligned}
    E^{*}  = \frac{1}{y^{T}My} (\sigma y^{T}M & + My\sigma ^{T} - Hyy^{T}M - Myy^{T}H\\
    &- \frac{1}{y^{T}My} \left( y^{T}\sigma  - y^{T}Hy \right)Myy^{T}M).
\end{aligned}\] If the update matrix $E_t = E^{*} $ is used, the method is called the Greenstadt method with parameter $M$.

\begin{sectionbox}[BFGS method]
    The BFGS method is the Greenstadt method with parameter $M = H' = H_{t}^{-1}$ in step $t$, in which case $My = H'y = \sigma $ and the update matrix $E^{*} $ assumes the form
    \[\begin{aligned}
        E^{*}  &= \frac{1}{y^{T}\sigma } \left( -Hy\sigma ^{T} - \sigma y^{T}H + \left( 1 + \frac{y^{T}Hy}{y^{T}\sigma } \right) \sigma \sigma ^{T} \right)$
    \end{aligned}\] where $H = H_{t-1}^{-1}$, $\sigma =x_{t} - x_{t-1}$, $y = \nabla f\left( x_t \right) - \nabla f\left( x_{t-1} \right)$.

\end{sectionbox}


\paragraph{Computational complexity of Quasi-Newton methods}

Newton's method requires matrix inversion, so has a cost of $\mathcal{O} \left( d^{3} \right)$ per iteration. Any method in the Greenstadt family avoids inversion, so is probably faster. In the BFGS method, the cost drops to $\mathcal{O} \left( d^{2} \right)$.

The L-BFGS method has runtime $\mathcal{O} \left( md \right)$ for recursion depth parameter $m$, which can be faster.

% \begin{color}{gray}
%     TODO: Finish
% \end{color}
