\section{Stochastic Optimization}

\begin{color}{violet}
    Description of SGD takes a bunch of context but in the end is quite self-explanatory.
\end{color}

\begin{sectionbox}[Stochastic Gradient Descent is unbiased]
    In SGD, gradients are unbiased, so we have $\mathbb{E} \left[ \nabla f\left( x_t, \xi _t \right) | x_t \right]  = \nabla F\left( x_t \right)$.
\end{sectionbox}

\begin{sectionbox}[Convergence (SGD in smooth nonconvex setting)]
    For the problem $\min _{x \in X} F\left( x \right) := \mathbb{E} \left[ f\left( x, \xi  \right) \right] $ where $F$ is $L$-smooth and SGD has bounded variance $\mathbb{E} \left[ \left\Vert \nabla f\left( x, \xi  \right) - \nabla F\left( x \right) \right\Vert _2^{2} \right]  \le \sigma ^{2}$. Then SGD with stepsize $\gamma _t = \min \left\{ \frac{1}{L}, \frac{\gamma _0}{\sigma \sqrt{T} } \right\}$ achieves
       $ \mathbb{E} \left[ \left\Vert \nabla F\left( \hat{x}_T \right) \right\Vert ^{2} \right]  &\le \frac{2L\left( F\left( x_1 \right) - F\left( x^{*}  \right) \right)}{T} + \frac{\sigma }{\sqrt{T} } \left( \frac{2\left( F\left( x_1 \right) - F\left( x^{*}  \right) \right)}{\gamma _0} + L\gamma _0 \right)$
    where $\hat{x}_T$ is selected uniformly at random from $\left\{ x_1, \dots, x_T  \right\}$.
\end{sectionbox}


    \emph{Proof.} $L$-smoothness of $F$ gives us
    \[\begin{aligned}
        \mathbb{E} \left[ F\left( x_{t+1}  \right) - F\left( x_t \right) \right] &\le  \mathbb{E} [ \nabla F\left( x_t \right)^{T} \left( x_{t+1} - x_t \right) \\
        & + \frac{L}{2} \left\Vert x_{t+1} - x_t \right\Vert ^{2} ] \\
       \text{(SGD step definition)}  &= \mathbb{E} [ -\gamma _t \nabla F\left( x_t \right)^{T} \nabla f\left( x_t, \xi _t \right)  \\
       &+ \frac{L\gamma _t^{2}}{2} \left\Vert \nabla f\left( x_t, \xi _t \right) \right\Vert ^{2} ]
    \end{aligned}\]
    Now we invoke unbiasedness $\mathbb{E} \left[ \nabla f\left( x_t, \xi _t \right) | x_t \right] = \nabla F\left( x_t \right)$ and bounded variance $\mathbb{E} \left[ \left\Vert \nabla f\left( x_t, \xi _t \right) \right\Vert ^{2} | x_t \right]  \le \sigma ^{2} + \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2}_2   $ and get
    \[\begin{aligned}
        \mathbb{E} \left[ F\left( x_{t+1} \right)  - F\left( X_t \right) \right]  &\le - \left( \gamma _t - \frac{L\gamma _t^{2}}{2} \right) \mathbb{E} \left\Vert \nabla F\left( x_t \right) \right\Vert ^{2} \\
        & + \frac{L\sigma ^{2}\gamma _t^{2}}{2}\\
       \text{($\gamma _t \le \frac{1}{L}$ so $\gamma _t - \frac{L\gamma _t^{2}}{2} \ge \gamma _t/2$)}  &\le  -\frac{\gamma _t}{2} \mathbb{E} \left\Vert \nabla F\left( x_t \right) \right\Vert ^{2} + \frac{L\sigma ^{2}\gamma _t^{2}}{2}
    \end{aligned}\]
    The rest is telescoping sum and definition of $\hat{x}_T$ and $\gamma _t = \min \left\{ \frac{1}{L}, \frac{\gamma _0}{\sigma \sqrt{T} } \right\}$
     $   \mathbb{E} \left[ \left\Vert \nabla F\left( \hat{x}_T \right) \right\Vert ^{2} \right]  &= \frac{1}{T} \sum_{t=1}^{T}  \mathbb{E} \left\Vert \nabla F\left( x_t \right) \right\Vert ^{2} \le  \frac{2\left( F\left( x_1 \right) - F\left( x^{*}  \right) \right)}{\gamma T} + \gamma \sigma ^{2}L$
    Now using definition of $\gamma _t$ we reach desired conclusion.

\begin{sectionbox}[Strong Growth Condition]
    Under strong growth condition
      $  \mathbb{E} \left[ \left\Vert \nabla f\left( x, \xi  \right) \right\Vert _2^{2} \right]  &\le c\left\Vert \nabla F\left( x \right) \right\Vert ^{2}_2$
      SGD with constant stepsize converges to the global optimum at a linear rate. Modern overparametrized ML models falling into the interpolation regime often satisfy SGC and enjoy fast linear convergence.
\end{sectionbox}


% \begin{color}{gray}
%     TODO: Skipped all proofs :(
% \end{color}

\begin{notebox}[Complexity of GD vs SGD]
    For $L$-smooth and $\mu $-strongly convex problems, let $\kappa = L/\mu $
    \begin{center}\begin{tabular}{ |P{1cm} | P{1.4cm} | P{0.7cm} | P{0.9cm} | }
        \hline
       Convex  & iteration complexity & iteration cost & total \\ \hline
       GD & $\mathcal{O} \left( \kappa \log \left( 1/\varepsilon  \right) \right)$ & $\mathcal{O} \left( n \right)$ & $\mathcal{O} \left( n\kappa \log \left( 1/\varepsilon   \right) \right)$\\ \hline
       SGD & $\mathcal{O} \left( 1/\varepsilon  \right)$ & $\mathcal{O} \left( 1 \right)$ &$\mathcal{O} \left( 1/\varepsilon  \right)$\\ \hline
       Nonconvex &  &  & \\ \hline
       GD & $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ & $\mathcal{O} \left( n \right)$ & $\mathcal{O} \left( n/\varepsilon ^{2} \right)$\\ \hline
       SGD & $\mathcal{O} \left( 1/\varepsilon ^{4} \right)$ & $\mathcal{O} \left( 1 \right)$ & $\mathcal{O} \left( 1/\varepsilon ^{4} \right)$ \\   \hline
    \end{tabular}\end{center}
\end{notebox}



\section{Finite Sum Optimization / Variance-Reduced Algorithms}

\begin{color}{gray}
    Sorry I skipped this part :(
\end{color}

% \begin{color}{gray}
%     TODO: Skipping for now :(
% \end{color}
