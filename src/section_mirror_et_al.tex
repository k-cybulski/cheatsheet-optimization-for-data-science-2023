\section{Mirror Descent, Smoothing, Proximal Algorithms}

\begin{sectionbox}[Bregman divergence]

    Let $\omega\left( x \right) : X \to \mathbb{R} $ be a function that is strictly convex, continuously differentiable on a closed convex set $X$. The Bregman divergence is defined as
    \[\begin{aligned}
        V_{\omega }\left( x, y \right) = \omega \left( x \right) - \omega \left( y \right) - \nabla \omega \left( y \right)^{T} \left( x - y \right)
    \end{aligned}\] for all $x, y \in X$.

    We call $\omega \left( \cdot \right)$ a distance-generating function. If $\omega \left( x \right)$ is $\sigma $-strongly convex w.r.t. some norm with modulus $\sigma $ so that $\omega \left( x \right) \ge \omega \left( y \right) + \nabla \omega \left( y \right)^{T} \left( x - y \right) + \frac{\sigma }{2    } \left\Vert x - y \right\Vert ^{2}$, then we always have $V_{\omega } \left( x, y \right) \ge \frac{\sigma }{2 } \left\Vert x - y \right\Vert ^{2}$.
\end{sectionbox}


\begin{sectionbox}[Generalized Pythagorean Theorem]

    If $x^{*} $ is the Bregman projection of $x_0$ onto a convex set $C \subseteq X : x^{*}  = \arg \min _{x\in C} V_{\omega }\left( x, x_0 \right)$, then for all $y \in C$ it holds that
$        V_{\omega }\left( y, x_0 \right) &\ge V_{\omega }\left( y, x^{*}  \right) + V_{\omega } \left( x^{*} , x_0 \right).$
\end{sectionbox}



\begin{sectionbox}[Prox-mapping for Mirror Descent]
    Given an input $x$ and vector $\xi $, we define the prox-mapping
       $ \text{Prox} _x \left( \xi  \right) = \arg \min _{u \in X} \left\{ V_{\omega }\left( u, x \right) + \left\langle \xi , u \right\rangle  \right\}$ where the distance-generating function $\omega \left( \cdot \right)$ is 1-strongly convex with respect to the norm $\left\Vert \cdot \right\Vert $ on $X$.
\end{sectionbox}


\begin{sectionbox}[Mirror Descent]
    The Mirror Descent algorithm takes a subgradient $g_t \in \partial f\left( x_t \right)$ and uses the update
    \[\begin{aligned}
        x_{t+1} &= \text{Prox} _{x_t} \left( \gamma _t g_t \right) =\arg \min _{x \in X} \left\{ V_{\omega } \left( x, x_t \right) + \left\langle \gamma _t g_t, x \right\rangle  \right\}\\
        &= \arg \min _{x \in X} \left\{ \omega \left( x \right) + \left\langle \gamma _t g_t - \nabla \omega \left( x_t \right), x \right\rangle  \right\}
    \end{aligned}\]
\end{sectionbox}


\paragraph{Mirror Descent with $l_2$ is Subgradient Descent}

On an $l_2$ space with $\omega \left( x \right) = \frac{1}{2  }\left\Vert x \right\Vert _2^{2}$ and norm $\left\Vert \cdot \right\Vert  = \left\Vert \cdot \right\Vert _2$, Mirror Descent reduces to Subgradient Descent.


% \begin{color}{gray}
%     TODO: Example of $l_1$ setup for mirror descent
% \end{color}

\begin{sectionbox}[Three point identity]
    For any $x, y, z \in \text{dom} \left( \omega  \right) $
    \[\begin{aligned}
        V_{\omega }\left( x, z \right) = V_{\omega }\left( x, y \right) + V_{\omega }\left( y ,z \right) - \left\langle \nabla \omega \left( z \right) - \nabla \omega \left( y \right), x - y \right\rangle
    \end{aligned}\]

    When $\omega \left( x \right) = \frac{1}{2} \left\Vert x \right\Vert _2^{2}$ this is the same as law of cosines.
\end{sectionbox}


\begin{sectionbox}[Optimality condition with $\omega$]
    Since $x_{t+1} = \arg \min _{x \in X} \left\{ \omega \left( x \right) + \left\langle \gamma _tg_t - \nabla \omega \left( x_t \right), x \right\rangle  \right\}$, by optimality condition it is true for all $x \in X$ that
    \[\begin{aligned}
        \left\langle \nabla \omega \left( x_{t+1} \right) + \gamma _t g_t - \nabla \omega \left( x_t \right) , x - x_{t+1} \right\rangle  \ge 0.
    \end{aligned}\]
\end{sectionbox}


\begin{sectionbox}[Convergence for Mirror Descent]
    Let $f$ be convex and $\omega \left( \cdot \right)$ be 1-strongly convex on $X$ with regard to norm $\left\Vert \cdot \right\Vert $. Then we have
    \[\begin{aligned}
        \min _{1 \le t T} f\left( x_t \right) - f^{*}  &\le  \frac{V_{\omega }\left( x^{*} , x_1 \right) + \frac{1}{2} \sum_{t=1}^{T} \gamma _t^{2} \left\Vert g_t \right\Vert ^{2}_{*}}{\sum_{t=1}^{T} \gamma _t}\\
        f\left( \frac{\sum_{t=1}^{T}  \gamma _tx_t}{\sum_{t=1}^{T} \gamma _t} \right) - f^{*}  &\le  \frac{V_{\omega }\left( x^{*} , x_1 \right) + \frac{1}{2} \sum_{t=1}^{T}  \gamma _t^{2} \left\Vert g_t \right\Vert ^{2}_{*}}{\sum_{t=1}^{T} \gamma _t}
    \end{aligned}\] where $\left\Vert \cdot \right\Vert _{*}$ denotes dual norm: $\left\Vert y \right\Vert _{*} = \max _{x: \left\Vert x \right\Vert \le 1 } \left\langle y, x \right\rangle $.

\end{sectionbox}

\begin{color}{violet}
    Skipping convergence analysis. For smooth case, look at extras at the end
\end{color}


\begin{sectionbox}[Convex conjugate (Legendre-Fenchel transform)]
    For a function $f : \text{dom} \left( f \right)  \to \mathbb{R} $, its convex conjugate is given as
    \[\begin{aligned}
        f^{*} \left( y \right) = \sup _{x \in \text{dom} \left( f \right) } \left\{ x^{T}y - f\left( x \right) \right\}.
    \end{aligned}\]

    Note that $f$ need not be convex, but $f^{*} $ will always be convex since it's a supremum over linear functions of $y$.
\end{sectionbox}

\begin{sectionbox}[Fenchel inequality]
    The definition of convex conjugate $f^{*} $ implies that for all $x, y$
    \[\begin{aligned}
        f^{*} \left( y \right) &\ge x^{T}y - f\left( x \right)  \implies x^{T}y &\le f\left( x \right) + f^{*} \left( y \right).
    \end{aligned}\]
    The above inequality is the Fenchel inequality, which generalizes Young's inequality $   x^{T}y &\le  \frac{\left\Vert x \right\Vert ^{2}}{2} + \frac{\left\Vert y \right\Vert ^{2}_{*}}{2}$
\end{sectionbox}


\paragraph{Dual norm and convex conjugate}

For a function $f\left( x \right) = \frac{1}{2} \left\Vert x \right\Vert ^{2}$, the convex conjugate is the dual norm $f^{*} \left( y \right) = \frac{1}{2} \left\Vert y \right\Vert ^{2}_{*}$.

\begin{sectionbox}[Properties of convex conjugate]
    \begin{enumeratenosep}[(i)]
        \item If $f$ is convex, lower semi-continuous and proper, then $\left( f^{*}  \right)^{*}  = f$.

              Lower semi-continuity means that $\lim \inf _{x \to x_0} f\left( x \right) \ge f\left( x_0  \right)$, so the level set $\left\{ x:f\left( x \right) \le \alpha  \right\}$ of $f$ is a closed set. A proper convex function means that $f\left( x  \right) > -\infty$. So, for $f$ satisfying the above, $f\left( x \right)$ admits a Fenchel representation
              \[\begin{aligned}
                  f\left( x \right) = \max _{y \in \text{dom} \left( f^{*}  \right) } \left\{ y^{T}x - f^{*} \left( y \right) \right\}
              \end{aligned}\]
          \item If $f$ is $\mu $-strongly convex then $f^{*} $ is continuously differentiable and $\frac{1}{\mu }$-Lipschitz smooth.
          \item Let $f$ and $g$ be two proper, convex, semi-continuous functions. Then
    \begin{enumeratenosep}[(a)]
        \item  $\left( f + g \right)^{*} \left( x \right) = \inf _y \left\{ f^{*} \left( y \right) + g^{*} \left( X - y \right) \right\}$
        \item $\left( \alpha f \right)^{*} \left( x \right) = \alpha f^{*}  \left( \frac{x}{\alpha } \right)$ for $\alpha  > 0$.
    \end{enumeratenosep}
    \end{enumeratenosep}
\end{sectionbox}


% \begin{color}{gray}
%     TODO: Skipped Huber function.
% \end{color}

\subsection{Common smoothing techniques}
\begin{sectionbox}[Nesterov smoothing]
            \[\begin{aligned}
                f_{\mu }\left( X \right) = \max _{y \in \text{dom} \left( f^{*}  \right) } \left\{ x^{T}y - f^{*} \left( y \right) - \mu \cdot d\left( y \right) \right\}
            \end{aligned}\] where $f^{*} $ is convex conjugate and $d$ is some proximity function which is strongly convex and nonnegative everywhere. It satisfies
            \[\begin{aligned}
                f_{\mu }\left( X \right) = \left( f^{*}  + \mu d \right)^{*} \left( x \right).
            \end{aligned}\] By adding the strongly convex term $\mu d\left( y \right)$, the function $\left( f^{*}  + \mu d \right)$ is strongly convex, so by properties of convex conjugate, $f_{\mu }\left( x \right)$ is continuously differentiable and Lipschitz-smooth.
\end{sectionbox}
\begin{sectionbox}[Moreau-Yosida smoothing/regularization]
        \[\begin{aligned}
            f_{\mu }\left( x \right) = \min _{y \in \text{dom} \left( f \right) } \left\{ f\left( y \right) + \frac{1}{2\mu } \left\Vert x - y \right\Vert^{2}_2  \right\} = \text{prox} _{\mu f}\left( x \right)
        \end{aligned}\] where $\mu >0$ is the approximation parameter.

        Note: Under $d\left( y \right) = \frac{1}{2} \left\Vert y \right\Vert ^{2}_2$, this is equivalent to Nesterov smoothing
\end{sectionbox}
\begin{sectionbox}[Randomized smoothing]
\[\begin{aligned}
    f_{\mu }\left( x  \right) = \mathbb{E} _Z f\left( x + \mu Z \right)
\end{aligned}\] where $Z$ is an isotopic Gaussian or uniform random variable.
\end{sectionbox}


\begin{sectionbox}[Proximity function for Nesterov]
    A proximity function $d \left( y \right)$ in Nesterov smoothing should satisfy the following properties
    \begin{enumeratenosep}[(i)]
        \item $d\left( y \right)$ is continuous and 1-strongly convex on $Y$,
        \item $d\left( y_0 \right) = 0$ for $y_0 \in \arg \min _{y \in Y}d\left( y \right)$,
        \item $d\left( y \right) \ge 0$ for all $y \in Y$.
    \end{enumeratenosep}

\end{sectionbox}

\begin{sectionbox}[Guarantees for Nesterov Smoothing]
    For $f_{\mu }\left( x \right)$, we have
    \begin{enumeratenosep}[(i)]
        \item $f_{\mu }\left( x \right)$ is continuously differentiable
        \item $\nabla f_{\mu }\left( x \right) = A^{T}y\left( x \right)$ where $y\left( x \right) = \arg \max _{y \in Y} \left\{ \left\langle Ax + b, y \right\rangle  - \phi \left( y \right) - \mu d\left( y \right) \right\}$
        \item $f_{\mu }\left( x \right)$ is $\frac{\left\Vert A \right\Vert ^{2}_2}{\mu }$-Lipschitz smooth, where $\left\Vert A \right\Vert _2 := \max _{x: \left\Vert x \right\Vert _2 \le 1} \left\Vert Ax \right\Vert _2$.
        \item For any $\mu  > 0$ let $D^{2}_Y = \max _{y \in Y}d\left( y \right)$. Then $ f\left( x \right) - \mu D^{2}_Y &\le f_{\mu }\left( x \right) \le f\left( x \right).$
    \end{enumeratenosep}

\end{sectionbox}

\paragraph{ GD convergence with Nesterov smoothing}
Applying projected gradient descent to solve the smooth problem results in
 $   f\left( x_t \right) - f\left( x^{*}  \right) &\le \mathcal{O} \left( \frac{\left\Vert A \right\Vert _2^{2}D^{2}_X}{\mu t} + \mu D^{2}_Y \right).$

Applying accelerated gradient descent to the smooth problem has
 $   f\left( x_t \right) - f\left( x^{*}  \right) &= \mathcal{O} \left( \frac{\left\Vert A \right\Vert _2^{2}D^{2}_X}{\mu t^{2}} + \mu D^{2}_Y \right). $

 So, to get an error less than threshold $\varepsilon $, we need to set $\mu  = \mathcal{O} \left( \frac{\varepsilon }{D^{2}_Y} \right)$, and the total number of iterations is then at most $T_{\varepsilon }= \mathcal{O} \left( \frac{\left\Vert A \right\Vert _2 D_XD_Y}{\varepsilon } \right)$ which is much better than $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ which we would get from directly applying subgradient descent.

% \begin{color}{gray}
%     TODO: Skip remarks on how Nesterov affects gradient descent.
% \end{color}

% \begin{color}{gray}
%     TODO: I skipped a bunch of things here...
% \end{color}

\begin{sectionbox}[Proximal operators]
Given a convex function $f$, the proximal operator of $f$ at a given point $x$ is defined as
\[\begin{aligned}
    \text{prox} _f\left( x \right) &= \arg \min _{y} \left\{ f\left( y \right) + \frac{1}{2} \left\Vert x - y \right\Vert^{2} \right\}.
\end{aligned}\] Immediately this implies, for any $\mu >0$, we have
\[\begin{aligned}
    \text{prox} _{\mu f}\left( x \right) &= \arg \min _{y} \left\{ f\left( y \right) + \frac{1}{2\mu } \left\Vert x - y \right\Vert ^{2} \right\}.
\end{aligned}\]

    \paragraph{Properties of proximal operators}
    \begin{enumeratenosep}[(a)]
        \item \emph{Fixed Point}. A point $x^{*} $ minimizes $f\left( x \right)$ iff $x^{*}  = \text{prox} _f\left( x^{*}  \right)$.
        \item \emph{Non-expansive}. $\left\Vert \text{prox} _f\left( x \right) - \text{prox} _f \left( y \right) \right\Vert \le \left\Vert x - y \right\Vert $.
        \item \emph{Moreau Decomposition}. For any $x, x=\text{prox} _f \left( x \right) + \text{prox} _{f^{*} }\left( x \right)$.
        \item \emph{Subgradient characterization.} $y = \text{prox} _g\left( x \right) \iff x - y \in \partial g\left( y \right)$.
    \end{enumeratenosep}
\end{sectionbox}
\emph{Proof of subgradient characterization.} Given $x \in \mathbb{R} ^{d}$, we define $h\left( z \right) = g\left( z \right) + 1/2 \left\Vert x - z \right\Vert _2^{2}$. We know that $1/2 \left\Vert x-z \right\Vert _2^{2}$ is differentiable with gradient $z-x$ at $z \in \mathbb{R} ^{d}$, so we can use the subgradient rule \emph{conic combination} to get $\partial h\left( z \right) = \partial g\left( z \right) + \left\{ z - x \right\}$.

So $y = \text{prox} _g\left( x \right) \iff $ $y$ is a minimizer of $h$
$\iff 0 \in \partial h\left( y \right)$
$\iff \left( x - y \right) + \left( y - x \right) \in \partial h\left( y \right)$
$\iff x - y \in \partial g\left( y \right)$.

% \begin{color}{violet}
%     Skip other proofs.
% \end{color}


\begin{sectionbox}[Proximal Point Algorithm (PPA)]

    The goal is to minimize a non-smooth convex function $f\left( x \right)$. The proximal point algorithm has an update
    \[\begin{aligned}
        x_{t+1} = \text{prox} _{\gamma _t f}\left( x_t \right)
    \end{aligned}\] where $\gamma _t > 0$ are stepsizes.

    This is \emph{not} a gradient based algortihm.
\end{sectionbox}


\begin{sectionbox}[Convergence of PPA]
    For a convex function $f$, the proximal point algorithm satisfies
        $f\left( x_t \right) - f^{*}  \le  \frac{\left\Vert x_0 - x^{*}  \right\Vert ^{2}_2}{ 2\sum_{\tau =0}^{t - 1} \gamma _{\tau }}$.

    So, the algorithm converges so long as $\sum_{t}^{} \gamma _t \to \infty$. However, the cost of the proximal operator may increase for larger $\gamma _t$.

\end{sectionbox}

\emph{Proof.} By optimality of $x_{t+1}$

 $   f\left( x_{t+1} \right) + \frac{1}{2\gamma _{t}} \left\Vert x_{t+1} - x_t \right\Vert _2^{2} &\le f\left( x_t \right)$

 $   f\left( x_t \right) - f\left( x_{t+1} \right) &\ge  \frac{1}{2\gamma _{t}} \left\Vert x_{t+1} - x_t \right\Vert ^{2}_2.$

This implies that $f\left( x_t \right)$ is non-increasing. Take subgradient $g \in \partial f\left( x_{t+1} \right)$, which by definition satisfies
   $ f\left( x_{t+1}  \right) - f^{*}  \le g^{T} \left( x_{t+1} - x^{*}  \right).$

From optimality condition of $x_{t+1}$ we have
\[\begin{aligned}
    0 \in \partial f\left( x_{t+1} \right) + \frac{1}{\gamma _t} \left( x_{t+1} - x_t \right) \implies \frac{x_t - x_{t+1}}{\gamma _t} \in \partial f\left( x_{t+1} \right).
\end{aligned}\] So
\[\begin{aligned}
    f\left( x_{t + 1}  \right) - f^{*}  &\le  \frac{1}{\gamma _t}\left( x_t - x_{t+1} \right)^{T} \left( x_{t+1} - x^{*}  \right)\\
    &\le  \frac{1}{\gamma _t} \left( x_t - x^{*}  + x^{*}  - x_{t+1} \right) ^{T} \left( x_{t+1} - x^{*}  \right)\\
    &\le \frac{1}{\gamma _t} [ \left( x_t - x^{*}  \right)^{T}\left( x_{t+1} - x^{*}  \right) \\
    &- \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} ].
\end{aligned}\]
Since $\left( x_t - x^{*}  \right)^{T} \left( x_{t+1} - x^{*}  \right) \le \frac{1}{2} [ \left\Vert x_t - x^{*}  \right\Vert ^{2} + \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} ]$, this implies

 $   \gamma _t \left( f\left( x_{t+1} \right) - f^{*}  \right) \le \frac{1}{2} \left[ \left\Vert x_t - x^{*}  \right\Vert ^{2} - \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} \right]$

Summing up, we have


  $  \sum_{\tau =0}^{t - 1}  \gamma _{\tau }\left( f\left( x_{\tau +1} \right)-f^{*}  \right) \le \frac{\left\Vert x_0 - x^{*}  \right\Vert^{2} }{2} - \frac{\left\Vert x_t   -x^{*}\right\Vert^{2} }{2} \le  \frac{\left\Vert x_0 - x^{*}  \right\Vert ^{2}}{2}.$

Since $f\left( x_{\tau } \right)$ is non-increasing, we have
\[\begin{aligned}
    \left( \sum_{\tau =0}^{t - 1} \gamma _{\tau } \right)\left( f\left( x_t \right) - f^{*}  \right) &\le  \sum_{\tau =0}^{t - 1} \gamma _{\tau }\left( f\left( x_{\tau +1} \right)-f^{*}  \right)
\end{aligned}\] and so
$    f\left( x_t \right) - f^{*}  &\le \frac{\left\Vert x_0 - x^{*}  \right\Vert ^{2}}{2\sum_{\tau =0}^{t-1} \gamma _{\tau }}$
