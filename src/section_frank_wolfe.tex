\section{Frank-Wolfe algorithm}

\begin{sectionbox}[Linear Minimization Oracle (LMO)]

    For the feasible region $X \subseteq \mathbb{R} ^{d}$ and an arbitrary vector $g \in \mathbb{R} ^{d}$, the LMO: $ \text{LMO} _X \left( g \right) := \arg \min _{z \in X}g^{T}z$ is any minimizer of the linear function $g^{T}z$ over $X$. If $X$ is closed and bounded, this is guaranteed to exist.

\end{sectionbox}

\begin{sectionbox}[Frank-Wolve algorithm]
    The Frank-Wolfe algorithm proceeds iteratively, starting from an initial feasible point $x_0 \in X$, using a time-dependent stepsize $\gamma _t \in \left[ 0, 1 \right]$.
    \[\begin{aligned}
        s &:= \text{LMO} _X \left( \nabla f\left( x_t \right) \right)\\
        x_{t+1} &:= \left( 1 - \gamma _t \right)x_t + \gamma _t s
    \end{aligned}\]
\end{sectionbox}



\begin{sectionbox}[Extreme points and atoms]
    For a convex set $X$, atoms $\mathcal{A} $ are a set such that $X$ is the convex hull of $\mathcal{A} $: $\text{conv} \left( \mathcal{A}  \right) = X$. There is always at least one atom in a solution to the Linear Minimization Oracle.

    The optimal set of atoms is the set of extreme points. A point $x \in X$ is extreme if $x \notin \text{conv} \left( X \setminus \left\{ x \right\} \right)$. There can also be suboptimal sets of atoms, like $\mathcal{A}  = X$.
\end{sectionbox}



\paragraph{LMO for unit $l_1$ ball}

\[\begin{aligned}
    \text{LMO} _X \left( g \right) &=\arg \min _{z \in X} z^{T}g\\
    &=\arg \min _{z \in \left\{ \pm e_1, \dots, \pm e_n \right\}} z^{T}g\\
    &= -\text{sgn} \left( g_i \right)e_i \text{ with } i:=\arg \max _{i \in \left[ d \right]} \left| g_i \right|
\end{aligned}\]


% \begin{color}{violet}
%     Skipping Hazan's algorithm
% \end{color}


\begin{sectionbox}[Duality gap]
    Given $x \in X$, we define the duality gap at $x$ as
    \[\begin{aligned}
        g\left( x \right) := \nabla f\left( x \right)^{T} \left( x - s \right) \text{ for } s:=\text{LMO} _X\left( \nabla f\left( x \right) \right)
    \end{aligned}\]

    Suppose that a constrained minimization problem of minimizing convex $f$ over convex $X$ has a minimizer $x^{*}  $. Let $x\in X$. Then the duality gap $g$ satisfies $g\left( x \right) \ge f\left( x \right) - f\left( x^{*}  \right) $so it's an upper bound for the optimality gap.

\end{sectionbox}
    \emph{Proof.} Since $s$ minimizes $\nabla f\left( x \right)^{T}z$ over $X$
    \[\begin{aligned}
        g\left( x \right) &= \nabla f\left( x \right)^{T} \left( x - s \right) \ge  \nabla f\left( x \right)^{T} \left( x - x^{*}  \right) \ge  f\left( x \right) - f\left( x^{*}  \right)
    \end{aligned}\] using first-order characterization of convexity of $f$


\begin{sectionbox}[Decrease lemma for Frank-Wolfe]
    For a step $x_{t+1} := x_t + \gamma _t \left( s - x_t \right)$ with stepsize $\gamma _t \in \left[ 0, 1 \right]$, it holds that
     $   f\left( x_{t+1} \right) &\le  f\left( x_t \right) - \gamma _t g\left( x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}$ where $s = \text{LMO} _X \left( \nabla f\left( x_t \right) \right)$.

\end{sectionbox}

\emph{Proof.} From smoothness of $f$, we have
\[\begin{aligned}
    f\left( x_{t+1} \right) &= f\left( x_t + \gamma _t \left( s - x_t \right) \right)\\
    &\le f\left( x_t \right) + \nabla f\left( x_t \right)^{T} \gamma _t\left( s - x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}\\
    &= f\left( x_t \right) - \gamma _t g\left( x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}.
\end{aligned}\]

\begin{sectionbox}[Convergence analysis (naive)]
    If $f$ is convex and smooth with parameter $L$, $X$ is convex, closed, and bounded. With any $x_0 \in X$ and with stepsizes $\gamma _t = 2/\left( t + 2 \right)$, Frank-Wolfe algorithm yields
    \[\begin{aligned}
        f\left( x_T \right) - f\left( x^{*}  \right) &\le \frac{2L \text{diam} \left( X \right)^{2}}{T + 1}
    \end{aligned}\] for $T \ge 1$ where $\text{diam} \left( X \right) :=\max _{x, y \in X} \left\Vert x - y \right\Vert $ is the diameter of $X$ (which exists since $X$ is closed and bounded).

\end{sectionbox}

\emph{Proof.} Writing $h\left( x \right) := f\left( x \right) - f\left( x^{*}  \right)$ for the optimization gap at point $x$, and using the certificate property of duality gap that $h\left( x \right) \le g\left( x \right)$, the decrease lemma implies that
\[\begin{aligned}
    h\left( x_{t+1} \right) &\le  h\left( x_t \right) - \gamma _t g\left( x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}\\
    &\le  h\left( x_t \right) - \gamma _t h\left( x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}\\
    &= \left( 1 - \gamma _t \right) h\left( x_t \right) + \gamma _t^{2} \frac{L}{2} \left\Vert s - x_t \right\Vert ^{2}\\
    &\le  \left( 1 - \gamma _t \right)h\left( x_t \right) + \gamma _t^{2}C
\end{aligned}\] where $C = \frac{L}{2} \text{diam} \left( X \right)^{2}$.

The convergence proof finishes by induction. It can be shown that $h\left( x_t \right) \le \frac{4C}{t + 1}$.

% \begin{color}{violet}
%     Skipping alternative stepsizes...
% \end{color}


\begin{sectionbox}[Affine invariance]
    Frank-Wolfe is invariant to affine transformations. It incurs the same optimization error on problem $f\left( x \right)$ as on problem $f'\left( x \right) = f\left( Ax + b \right)$ after the same number of steps.
\end{sectionbox}


\begin{sectionbox}[Curvature constant]
    Curvature constant is an affine-invariant measure of complexity of an objective function and its constraint set.
    \[\begin{aligned}
            C_{\left( f, \mathcal{X}  \right)} &:= \sup _{\substack{x,s \in \mathcal{X} , \gamma  \in \left( 0, 1 \right] \\ y = \left( 1 - \gamma  \right)x + \gamma s}     } \frac{1}{\gamma ^{2}} \left( f\left( y \right) - f\left( x \right) - \nabla f\left( x \right)^{T} \left( y - x \right) \right).
    \end{aligned}\]

    When $f$ is twice differentiable, $C_{\left( f, X \right)}$ is always bounded by the constant $C \frac{L}{2} \text{diam} \left( X \right)^{2}$. So, an affine-invariant bound using the curvature constant can always be better than the standard convergence bound.
\end{sectionbox}


\begin{sectionbox}[Convergence analysis (curvature const)]
    With any $x_0 \in X$ and stepsizes $\gamma _t = 2/\left( t + 2 \right)$, the Frank-Wolfe algorithm yields
\[\begin{aligned}
    f\left( x_T \right) - f\left( x^{*}  \right) &\le  \frac{4C_{\left( f, X \right)}}{T + 1}.
\end{aligned}\]
\end{sectionbox}
To show this, we apply smoothness with $x:=x_t$, $y:=x_{t+1} = \left( 1 - \gamma _t \right)x_t + \gamma _ts$, $y - x = -\gamma _t \left( x - s \right)$ and the definition of $C_{\left( f, X \right)}$ to write
\[\begin{aligned}
    f\left( y \right) &\le  f\left( x \right) + \nabla f\left( x_t \right)^{T} \left( y - x \right) + \gamma _t^{2} C_{\left( f, X \right)}.
\end{aligned}\] We can then follow the proof as in the simple naive convergence proof.


% \begin{color}{violet}
%     Skip \emph{Sparsity, extensions and use cases}.
% \end{color}
