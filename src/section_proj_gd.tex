\section{Projected Gradient Descent}
\begin{sectionbox}[Projected Gradient Descent]
    In projected gradient descent, we first do a usual step of gradient descent, and then a projection operation.
    \[\begin{aligned}
        y_{t+1} &:= x_t - \gamma \nabla f\left( x_t \right)\\
        x_{t+1} &:= \Pi _X \left( y_{t+1} \right) := \arg \min _{x \in X} \left\Vert x - y_{t+1} \right\Vert ^{2}
    \end{aligned}\]
\end{sectionbox}


\begin{sectionbox}[Properties of projection operator]
    Let $X \subseteq \mathbb{R} ^{d}$ be closed and convex, $x \in X, y \in \mathbb{R} ^{d}$. Then
    \begin{enumeratenosep}[(i)]
        \item Obtuse angle: $\left( x - \Pi _X \left( y \right) \right)^{T} \left( y - \Pi _X\left( y \right) \right) \le 0$,
        \item $\left\Vert x - \Pi _X \left( y \right) \right\Vert ^{2} + \left\Vert y - \Pi _X\left( y \right) \right\Vert ^{2} \le \left\Vert x - y \right\Vert ^{2}$.
        \item Non-expansive: $\left\Vert \Pi _X \left( x \right) - \Pi _X\left( y \right) \right\Vert _2 \le \left\Vert x - y \right\Vert _2$
    \end{enumeratenosep}
\end{sectionbox}


\paragraph{Complexity of projection onto $l_1$-ball}

Let $v \in \mathbb{R} ^{d}$, $R \in \mathbb{R} _+$, $X = B_1\left( R \right)$ the $l_1$-ball around 0 of radius $R$. The projection
\[
    \Pi _X \left( v \right) = \arg \min _{x \in X} \left\Vert x - v \right\Vert ^{2}
\] of $v$ onto $B_1\left( R \right)$ can be computed in time $\mathcal{O} \left( d \log d \right)$.


\paragraph{Sufficient decrease (for projected GD).}
Choosing a stepsize $\gamma := \frac{1}{L}$ with projected gradient descent results in
\[\begin{aligned}
    f\left( x_{t+1} \right) &\le f\left(x_t \right) - \frac{1}{2L} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} + \frac{L}{2} \left\Vert y_{t+1} - x_{t+1} \right\Vert ^{2}.
\end{aligned}\]

\paragraph{Convergence analysis (for projected GD).}

Analysis is very similar to vanilla, but we need to replace $x_{t+1}$ by $y_{t+1}$, and use property (ii) of projection operators with $x = x^{*} , y = y_{t+1}$ to get $\left\Vert x_{t+1} - x^{*}  \right\Vert^{2}\le \left\Vert y_{t+1} - x^{*}  \right\Vert ^{2}$. Then
\[\begin{aligned}
    g_t^{T}\left( x_t - x^{*}  \right) &= \frac{1}{2\gamma }\left( \gamma ^{2} \left\Vert g_t \right\Vert ^{2} + \left\Vert x_t - x^{*}  \right\Vert ^{2} - \left\Vert y_{t+1} - x^{*}  \right\Vert ^{2} \right)\\
    &\le \frac{1}{2\gamma }\left( \gamma ^{2} \left\Vert g_t \right\Vert ^{2} + \left\Vert x_t - x^{*}  \right\Vert ^{2} - \left\Vert x_{t+1}- x^{*}  \right\Vert ^{2} \right)
\end{aligned}\] and we can continue the standard analysis.


% \begin{color}{violet}
%     Skipping a bunch of things
% \end{color}

\section{Coordinate Descent}

From \textcolor{blue}{\emph{you should know}} :
\begin{enumeratenosep}[a.]
    \item PL inequality is implied by strong convexity
    \item coordinate descent has a smaller cost per iteration than standard gradient descent, but it usually takes a higher number of iterations
    \item there are cases where coordinate descent leads to net speedups
    \item greedy coordinate descent may fail to converge in non-differentiable cases, but it converges in the separable case
\end{enumeratenosep}


\begin{sectionbox}[Polyak-Łojasiewicz inequality]

    Let $f : \mathbb{R} ^{d} \to \mathbb{R}$ be a differentiable function with a global minimum $x^{*}$. We say that $f$ satisfies the Polyak-Łojasiewicz inequality (PL inequality) if the following holds for some $\mu  > 0$:
 $       \frac{1}{2} \left\Vert \nabla f\left( x \right) \right\Vert ^{2} &\ge \mu \left( f\left( x \right) - f\left( x^{*}  \right) \right)$ for all $x \in \mathbb{R} ^{d}$. It is implied by strong convexity.
\end{sectionbox}


\begin{sectionbox}[GD convergence (smooth and PL inequality)]
    For a function $f$ which is smooth with parameter $L$ and satisfies the PL inequality  with parameter $\mu $, gradient descent with stepsize $\gamma  = \frac{1}{L}$ for arbitrary $x_0$ satisfies
       $ f\left( x_T \right) - f\left( x^{*}  \right) &\le  \left( 1 - \frac{\mu }{L} \right)^{T} \left( f\left( x_0 \right) - f\left( x^{*}  \right) \right).$
\end{sectionbox}


\emph{Proof.} For all $t$, we have
\[\begin{aligned}
    f\left( x_{t+1} \right) &\le  f\left( x_t \right) - \frac{1}{2L} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} &\text{(Suff. decrease)} \\
    &\le  f\left( x_t \right) - \frac{\mu }{L} \left( f\left( x_t \right) - f\left( x^{*}  \right) \right) &\text{(PL ineq.)}
\end{aligned}\]
Subtracting $f\left( x^{*}  \right)$ from both sides results in
\[\begin{aligned}
    f\left( x_{t+1} \right) - f\left( x^{*}  \right) &\le  \left( 1 - \frac{\mu }{L} \right) \left( f\left( x_t \right) - f\left( x^{*}  \right) \right).
\end{aligned}\] The statement follows from recursively applying the above inequality.

\begin{sectionbox}[Coordinate-wise smoothness]
    Let $f : \mathbb{R} ^{d} \to \mathbb{R} $ be differentiable, and $\mathcal{L}  = \left( L_1, \dots, L_d  \right) \in \mathbb{R} ^{d}_+$. Function $f$ is called coordinate-wise smooth (with parameter $\mathcal{L} $) if for every coordinate $i = 1, \dots, d$,
    \[\begin{aligned}
        f\left( x + \lambda \textbf{e} _i \right) &\le  f\left( x \right) + \lambda  \nabla _i f\left( x \right) + \frac{L_i}{2} \lambda ^{2}
    \end{aligned}\] for all $x \in \mathbb{R} ^{d}, \lambda  \in \mathbb{R} $. If $L_i = L$ for all $i$, $f$ is said to be coordinate-wise smooth with parameter $L$.
\end{sectionbox}


\begin{sectionbox}[Coordinate descent]
     In coordinate descent, the update rule is $        x_{t+1} := x_t - \gamma _i \nabla _i f\left( x_t \right)\textbf{e} _i.$
\end{sectionbox}


\paragraph{Sufficient decrease (coordinate-wise)}

If $f :\mathbb{R} ^{d}\to \mathbb{R} $ is differentiable and coordinate-wise smooth with parameter $\mathcal{L}  = \left( L_1, \dots, L_d  \right)$, the active coordinate in step $t$ is $i$, and stepsize $\gamma _i = \frac{1}{L_i}$, coordinate descent satisfies
\[\begin{aligned}
    f\left( x_{t+1}  \right) &\le  f\left( x_t \right) - \frac{1}{2L_i} \left| \nabla _if\left( x_t \right) \right|^{2}
\end{aligned}\]

\emph{Proof.} Apply coordinate-wise smoothness with $\lambda = -\nabla _i f\left( x_t \right)/L_i$ and $x_{t+1} = x_t + \lambda \textbf{e} _i$. Then

\[\begin{aligned}
    f\left( x_{t+1}  \right) &\le f\left( x_t \right) + \lambda \nabla _if\left( x_t \right) + \frac{L_i}{2}\lambda ^{2}\\
    &= f\left( x_t \right) - \frac{1}{L_i} \left| \nabla _i f\left( x_t \right) \right|^{2} + \frac{1}{2L_i} \left| \nabla _i f\left( x_t \right) \right|^{2}\\
    &= f\left( x_t \right) - \frac{1}{2L_i} \left| \nabla _i f\left( x_t \right) \right|^{2}.
\end{aligned}\]

\paragraph{Importance sampling}

In importance sampling, we sample $i \in \left[ d \right]$ with probability $\frac{L_i}{\sum_{j = 1}^{d} L_j}$.

% \begin{color}{gray}
%     TODO: Convergence and proof.
% \end{color}
