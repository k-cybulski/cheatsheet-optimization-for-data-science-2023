\section{Basics and convexity}

\paragraph{Basics}

\begin{enumeratenosep}[a.]
    \item Cosine theorem:
           $ 2\textbf{v}     ^{T}\textbf{w} = \left\Vert \textbf{v}  \right\Vert ^{2} + \left\Vert \textbf{w}  \right\Vert ^{2} - \left\Vert \textbf{v}  - \textbf{w}  \right\Vert ^{2}$
    \item Jensen's inequality:
        Let $f :\mathbb{R} ^{d} \to \mathbb{R} $ be a convex function, $x_1, \dots, x_m \in \text{dom} \left( f \right) $ and $\lambda _1, \dots, \lambda _m \in \mathbb{R} _+$ such that $\sum_{i = 1}^{m}  \lambda _i = 1.$ Then $f\left( \sum_{i = 1}^{m} \lambda _i x_i \right) \le \sum_{i = 1}^{m}  \lambda _if\left( x_i \right)$.
    \item Cauchy-Schwarz. For any $x, y$ in some space with an inner product and corresponding norm, $\left| \left\langle x, y \right\rangle  \right| \le \left\Vert x \right\Vert  \left\Vert y \right\Vert$
    \item Spectral norm: $\left\Vert A \right\Vert  = \max _{v \in \mathbb{R} ^{d}, v \neq 0} \frac{\left\Vert Av \right\Vert }{\left\Vert v \right\Vert } = \max _{\left\Vert v \right\Vert  = 1} \left\Vert Av \right\Vert $. It satisfies $\left\Vert Av \right\Vert  \le \left\Vert A \right\Vert \left\Vert v \right\Vert $ for all $v \in \mathbb{R} ^{d}$.
    \item $f$ is differentiable at $x$ in the interior of $\text{dom} \left( f \right) $ if there exists a matix $A$ and error function $r$ defined around $0$ such that for all $y$ in some neighbourhood of $x$:
        $f\left( y \right) = f\left( x \right) + A\left( y - x \right) + r\left( y - x \right)$
        where $\lim_{v \to 0} \frac{\left\Vert r\left( v \right) \right\Vert }{\left\Vert v \right\Vert }=0$.
\end{enumeratenosep}
\begin{sectionbox}[Convexity and smoothness]
    A function $f:\text{dom} \left( f \right) \to \mathbb{R} $ is convex if $\text{dom} \left( f \right) $ is convex and for all $x, y \in \text{dom} \left( f \right) $ and all $\lambda  \in \left[ 0, 1 \right]$ we have
    \[\begin{aligned}
        f\left( \lambda x + \left( 1 - \lambda  \right)y \right) &\le \lambda f\left( x \right) + \left( 1 - \lambda  \right)f\left( y \right)
    \end{aligned}\]
    \paragraph{Convexity and smoothness for diff. $f$}
    Let $f : \text{dom} \left( f \right) \to \mathbb{R} $ be a differentiable function, $X \subseteq \text{dom} \left( f \right) $ convex, and $L \in \mathbb{R} _+$. Over $X$, the function $f$ is called
    \begin{enumeratenosep}[a.]
    \item convex if (first-order char. of convexity)
       $ f\left( y \right) \ge  f\left( x \right) + \nabla f\left( x \right)^{T} \left( y - x \right),$
    \item strongly convex with parameter $\mu $ if
      $  f\left( y \right) \ge  f\left( x \right) + \nabla f\left( x \right)^{T} \left( y - x \right) + \frac{\mu }{2} \left\Vert x - y \right\Vert ^{2},$
        \item smooth with parameter $L$ if
        $f\left( y \right) \le f\left( x \right) + \nabla f\left( x \right)^{T} \left( y - x \right) + \frac{L}{2} \left\Vert x - y \right\Vert ^{2}$
    \end{enumeratenosep}
    for all $x, y \in X$.

    \paragraph{Second order characterization of convexity}
    If $\text{dom} \left( f \right) $ is open and $f$ is twice differentiable, with a symmetric Hessian, then $f$ is convex if and only if $\text{dom} \left( f \right) $ is convex, and for all $x \in \text{dom} \left( f \right) $ we have $\nabla ^{2} f\left( x \right) \succeq 0.$ (A symmetric matrix is positive semidefinite $M \succeq 0$ if $x^{T}Mx \ge 0$ for all $x$)

    \paragraph{Optimality condition for convex functions}

    Suppose that $f : \text{dom} \left( f \right)  \to \mathbb{R} $ is convex and differentiable over an open domain $\text{dom} \left( f \right) \subseteq \mathbb{R} ^{d}$, and let $X \subseteq \text{dom} \left( f \right) $ be a convex set. Point $x^{*}  \in X$ is a minimizer of $f$ over $X$ if and only if $\nabla f\left( x^{*}  \right)^{T} \left( x - x^{*}  \right) \ge 0$ for all $x \in X$.

    \paragraph{Operations which preserve convexity}

    \begin{enumeratenosep}[(i)]
        \item A linear combination with positive coefficients of convex functions is convex.
        \item If $f$ is a convex function and $g$ is an affine function ($g\left( x \right) = Ax + b$), then $f \circ g$ is convex.
    \end{enumeratenosep}

    \paragraph{Operations which preserve smoothness}

    \begin{enumeratenosep}[(i)]
        \item A linear combination of smooth functions $\sum_{i = 1}^{m} \lambda _i f_i$ is smooth with parameter $\sum_{i=1}^{m}  \lambda _iL_i$.
        \item If $f$ is smooth and $g\left( x \right) = Ax + b$ is affine, then $f\left( g\left( x \right) \right)$ is smooth with parameter $L\left\Vert A \right\Vert ^{2}$ in spectral norm.
    \end{enumeratenosep}



\end{sectionbox}

\paragraph{Constrained optimization with Lagrangians}

For an optimization problem $f_0\left( x \right)$ subject to $f_i\left( x \right) \le 0$, $h_i\left( x \right)=0$, the Lagrangian is
$L\left( x, \lambda , \nu  \right) = f_0\left( x \right) + \sum_{i = 1}^{m } \lambda _if_i\left( x \right) + \sum_{i = 1}^{p} \nu _i h_i\left( x \right)$. $\lambda _i$ and $\nu _i$ are called \emph{Lagrange multipliers}.

The Lagrange dual function is then $g\left( \lambda , \nu  \right) = \inf _{x \in \mathcal{D} }L \left( x, \lambda , \nu  \right)$

\emph{Weak Lagrange duality}. Let $x$ be a feasible solution, $\lambda  \in \mathbb{R} ^{m}, \lambda \ge 0, \nu \in \mathbb{R} ^{p}$. Then $g\left( \lambda , \nu  \right) \le f_0\left( x \right)$.

The Lagrange dual optimization problem is $\max g\left( \lambda , \nu  \right)$ subject to $\lambda  \ge 0$. This is always a convex problem (when we swap it to $\min -g$), even if the primal is not.

If there is a feasible solution $\tilde{x}$ which satisfies all inequality constraints strictly $f_i\left( \tilde{x}  \right) < 0$ (Slater point), then the infimum value of the primal equals the supremum value of its Lagrange dual. If this value is finite, then it is attained by a feasible solution of the dual.

\emph{Zero duality gap}. If $\tilde{x}$ is feasible for primal, $\left( \tilde{\lambda }, \tilde{\nu } \right)$ is feasible for dual, and $f_0\left( \tilde{x} \right) = g\left( \tilde{\lambda }, \tilde{\nu } \right)$, then we say there is zero duality gap.

\emph{Complementary slackness.} If $\tilde{x}$ and $\left( \tilde{\lambda }, \tilde{\nu } \right)$ have zero duality gap, then $\tilde{\lambda }_i f_i\left( \tilde{x} \right) = 0$ for $i=1, \dots, m$.

\emph{Vanishing Lagrangian gradient}. If $\tilde{x}$ and $\left( \tilde{\lambda }, \tilde{\nu } \right)$ have zero duality gap, and if all $f_i$ and $h_i$ are differentiable, then $\nabla f_0\left( \tilde{x} \right) + \sum_{i = 1}^{m}  \tilde{\lambda }_i \nabla f_i\left( \tilde{x} \right) + \sum_{i = 1}^{p } \tilde{\nu }_i \nabla h_i\left( \tilde{x}  \right) = 0$.

\emph{KKT conditions.} Let $\tilde{x}$ and $\left( \tilde{\lambda }, \tilde{\nu } \right)$ be feasible solutions for primal and dual:
\begin{enumeratenosep}[(i)]
\item \emph{Necessary}. If there is zero dualiy gap and all $f_i$ and $h_i$ are differentiable, then $\tilde{\lambda _i}f_i \left( \tilde{x}  \right) =0 $ for all $i$ and $\nabla f_0\left( \tilde{x} \right) + \sum_{i = 1}^{m} \tilde{\lambda }_i \nabla f_i\left( \tilde{x} \right) + \sum_{i = 1}^{p}  \tilde{\nu } _i \nabla h_i \left( \tilde{x} \right) = 0$.
\item \emph{Sufficient}. Suppose all $f_i$ and $h_i$ are differentiable, all $f_i$ are convex, all $h_i$ are affine, and the equalities given above in \emph{Necessary} hold. Then there is zero duality gap.
\end{enumeratenosep}


% \paragraph{Lagrange multipliers}
% (Theorem 9.1) Let $f : \mathbb{R} ^{d} \to \mathbb{R}     $ be convex and differentiable, $C \in \mathbb{R} ^{m \times d}$ for some $m \in \mathbb{N} $, $e \in \mathbb{R} ^{m}$, $x^{*}  \in \mathbb{R} ^{d}$ such that $Cx^{*}  = e$. Then the following two statements are equivalent
% \begin{enumerate}[(i)]
%     \item $x^{*}  = \arg \min \left\{ f\left( x \right) : x \in \mathbb{R} ^{d }, Cx = e \right\}$
%     \item There exists a vector $\lambda  \in \mathbb{R} ^{m}$ such that
%         \[\begin{aligned}
%             \nabla f\left( x^{*}  \right)^{T} = \lambda ^{T}C.
%         \end{aligned}\] The entries of $\lambda $ are known as the Lagrange multipliers.
% \end{enumerate}
