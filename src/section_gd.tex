\section{Gradient descent}

\begin{sectionbox}[Gradient descent]
    The gradient descent update rule is $ x_{t+1} = x_t - \gamma \nabla f\left( x_t \right)$.
\end{sectionbox}

\begin{sectionbox}[Sufficient decrease (for smooth $f$)]

    If $f: \mathbb{R} ^{d} \to \mathbb{R} $ is differentiable and smooth with parameter $L$, then gradient descent with learning rate $\gamma := \frac{1}{L}$ satisfies
    \[\begin{aligned}
        f\left( x_{t+1} \right) &\le  f\left( x_t \right) - \frac{1}{2L} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2}.
    \end{aligned}\] This also holds if $f$ is smooth with parameter $L$ over the line segment connecting $x_t$ and $x_{t+1}$.
\end{sectionbox}

    \emph{Proof.}
    Use definition of smoothness with $y:=x_{t+1}$, $x:=x_t$, and note that
        $x_{t+1} - x_t = -\gamma \nabla f\left( x_t \right).$ Then
    \[\begin{aligned}
        f\left( x_{t+1} \right) &\le f\left( x_t \right) - \nabla f\left( x_t \right)^{T} \left( x_{t+1} - x_t \right) + \frac{L}{2} \left\Vert x_{t+1} - x_t \right\Vert ^{2}\\
        &= f\left( x_t \right) - \frac{1}{L} \nabla f\left( x_t \right)^{T} \nabla f\left( x_t \right) + \frac{L}{2} \left\Vert \frac{1}{L} \nabla f\left( x \right) \right\Vert ^{2}\\
        &= f\left( x_t \right) - \frac{1}{2L} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2}
    \end{aligned}\]

\begin{notebox}[Convergence rates]
    \begin{center}\begin{tabular}{ |P{0.9cm} | P{0.7cm} | P{0.7cm} | P{0.7cm} | P{0.7cm}| }
       \hline & Lipschitz convex functions & smooth convex functions & strongly convex functions & smooth \& strongly convex functions \\ \hline
        gradient descent & $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ & $\mathcal{O} \left( 1/\varepsilon  \right)$ & & $\mathcal{O} \left( \log \left( 1/\varepsilon  \right) \right)$\\ \hline
        accelerated gradient descent & & $\mathcal{O} \left( 1/\sqrt{\varepsilon }  \right)$ & & \\ \hline
        projected gradient descent & $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ & $\mathcal{O} \left( 1/\varepsilon  \right)$ & & $\mathcal{O} \left( \log \left( 1/\varepsilon  \right) \right)$\\  \hline
        subgradient descent & $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ & & $\mathcal{O} \left( 1/\varepsilon  \right)$ & \\  \hline
        stochastic gradient descent & $\mathcal{O} \left( 1/\varepsilon ^{2} \right)$ & & $\mathcal{O} \left( 1/\varepsilon  \right)$ & \\
        \hline
    \end{tabular}\end{center}
\end{notebox}



\paragraph{Telescoping for gradient descent convergence.}

Applying first-order characterization of convexity of $f$, we can get
   $ f\left( x_t \right) - f\left( x^{*}  \right) &\le  \nabla f\left( x_t \right)^{T}\left( x_t - x^{*}  \right) &\text{$(*)$}$

Write $g_t := \nabla f\left( x_t \right)$, so $g_t = \left( x_t - x_{t+1} \right)/\gamma $.

Then
\[\begin{aligned}
    & g_t^{T}\left( x_t - x^{*}  \right) = \frac{1}{\gamma }\left( x_t - x_{t+1} \right)^{T} \left( x_t - x^{*}  \right)\\
    &= \frac{1}{2\gamma } \left( \left\Vert x_t - x_{t+1} \right\Vert ^{2} + \left\Vert x_t - x^{*}  \right\Vert  - \left\Vert x^{*}  - x_{t+1} \right\Vert  \right)\\
    &= \frac{1}{2\gamma }\left(  \gamma ^{2}\left\Vert g_t \right\Vert ^{2}   + \left\Vert x_t - x^{*}  \right\Vert ^{2} - \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2}\right)\\
    &= \frac{\gamma }{2} \left\Vert g_t \right\Vert ^{2} + \frac{1}{2\gamma } \left( \left\Vert x_{t} - x^{*}  \right\Vert ^{2}- \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} \right).
\end{aligned}\]
We can now do a telescoping sum
\[\begin{aligned}
    & \sum_{t=0}^{T - 1} g_t^{T} \left( x_t - x^{*}  \right)\\
     &= \frac{\gamma }{2} \sum_{t= 0}^{T - 1}  \left\Vert g_t \right\Vert ^{2} + \frac{1}{2\gamma } \left( \left\Vert x_0 - x^{*}  \right\Vert ^{2} - \left\Vert x_T - x^{*}  \right\Vert ^{2} \right)\\
    &\le \frac{\gamma }{2} \sum_{t = 0}^{T - 1}  \left\Vert g_t \right\Vert ^{2} + \frac{1}{2\gamma }\left\Vert x_0 - x^{*}  \right\Vert ^{2}.
\end{aligned}\]
So, coming back to $(*)$, this gives us a bound on the average error
\[\begin{aligned}
    \sum_{t = 0}^{T - 1}  \left( f\left( x_t \right) - f\left( x^{*}  \right) \right) &\le  \frac{\gamma }{2} \sum_{t=0}^{T - 1}  \left\Vert g_t \right\Vert ^{2} + \frac{1}{2\gamma } \left\Vert x_0 - x^{*}  \right\Vert ^{2}.
\end{aligned}\]

To use the above bound, we need to put extra assumptions to control for gradient norms $\left\Vert g_t \right\Vert ^{2}$.

\begin{sectionbox}[Convergence analysis (for smooth $f$)]

    For smooth functions with $\gamma = \frac{1}{L}$, we can use sufficient decrease to bound the sum of gradient norms $\left\Vert g_t \right\Vert ^{2}$
    \[\begin{aligned}
        \frac{1}{2L} \sum_{t = 0}^{T - 1}  \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} &\le  \sum_{t=0}^{T-1}\left( f\left( x_t \right) - f\left( x_{t+1} \right) \right) = f\left( x_0 \right) - f\left( x_T \right)
    \end{aligned}\]
    Combining the above with telescoping for GD, we get
    \[\begin{aligned}
        \sum_{t = 0}^{T - 1 }  \left( f\left( x_t \right) - f\left( x^{*}  \right) \right) &\le \frac{1}{2L} \sum_{t=0}^{T - 1} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} + \frac{L}{2} \left\Vert x_0 - x^{*}  \right\Vert ^{2}\\
        &\le  f\left( x_0 \right) - f\left( x_T \right) + \frac{L}{2} \left\Vert x_0 - x^{*}  \right\Vert ^{2}.
    \end{aligned}\]
    This is equivalent to $ \sum_{t=1}^{T}  \left( f\left( x_t \right) - f\left( x^{*}  \right) \right) &\le  \frac{L}{2} \left\Vert x_0 - x^{*}  \right\Vert ^{2}.$

    From sufficient decrease, we have $f\left( x_{t+1} \right) \le  f\left( x_t \right)$, so by taking the average, and knowing that the last term is best, we get
 $       f\left( x_T \right) - f\left( x^{*}  \right) &\le  \frac{1}{T} \sum_{t=1}^{T}  \left( f\left( x_t \right) - f\left( x^{*}  \right) \right)        \le \frac{L}{2T} \left\Vert x_0 - x^{*}  \right\Vert ^{2}.$

    So, to achieve $\min _{t=0}^{T - 1} \left( f\left( x_t \right) - f\left( x^{*}  \right) \right) \le \varepsilon $, we need $T \ge \frac{R^{2}L}{2\varepsilon }$ steps.
\end{sectionbox}


\begin{sectionbox}[Convergence rate for smooth and strongly convex]
    For smooth and strongly convex functions $f$ with smoothness parameter $L$ and strong convexity parameter $\mu $, gradient descent with stepsize $\gamma := \frac{1}{L}$ satisfies
    \begin{enumerate}[(i)]
        \item squared distances to $x^{*} $ are geometrically decreasing
            \[\begin{aligned}
                \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} &\le  \left( 1 - \frac{\mu }{L} \right) \left\Vert x_t - x^{*}  \right\Vert ^{2}
            \end{aligned}\]
        \item the absolute error after $T$ iterations is exponentially small in $T$
            \[\begin{aligned}
                f\left( x_t \right) - f\left( x^{*}  \right) &\le \frac{L}{2} \left( 1 - \frac{\mu }{L}  \right)^{T} \left\Vert x_0 - x^{*}  \right\Vert ^{2}
            \end{aligned}\]
    \end{enumerate}
\end{sectionbox}


\begin{sectionbox}[Convergence analysis (for strongly convex)]
$\mu $-strong convexity with reshuffling terms implies
$f\left( x_t \right) - f\left( x^{*}  \right) \le \left\langle \nabla f\left( x_t \right), x_t - x^{*}  \right\rangle  - \frac{\mu }{2} \left\Vert x_t - x^{*}  \right\Vert ^{2}$.

Now applying definition of $x_{t+1} = x_t - \gamma _t \nabla f\left( x_t \right)$ with cosine theorem gives us
$\left\langle \nabla f\left( x_t \right), x_t - x^{*}  \right\rangle  = \frac{1}{2\gamma _t} \left( \left\Vert x_t - x_{t+1} \right\Vert ^{2} + \left\Vert x_t - x^{*}  \right\Vert  - \left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} \right)$.

Ultimately, we get
$\left\Vert x_{t+1} - x^{*}  \right\Vert ^{2} \le 2\gamma _t \left( f\left( x^{*}  \right) - f\left( x_t \right) \right) + \gamma _t^{2} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} + \left( 1 - \mu \gamma _t \right)\left\Vert x_t - x^{*}  \right\Vert ^{2}.$

If we also have $L$-smoothness, then we can apply sufficient decrease to bound the terms
$f\left( x^{*}  \right) - f\left( x_t \right) \le f\left( x_{t+1} \right) - f\left( x_t \right) \le -\frac{1}{2L} \left\Vert \nabla f\left( x_t \right) \right\Vert ^{2}$

For $\gamma =1/L$, we can multiply by $2\gamma $ and rearrange terms to get
$2\gamma \left( f\left( x^{*}  \right) - f\left( x_t \right) \right) + \gamma ^{2}\left\Vert \nabla f\left( x_t \right) \right\Vert ^{2} \le 0$. This then gives us
$\left\Vert x_{t+1} - x^{*}  \right\Vert^{2} \le \left( 1 - \frac{\mu }{L} \right)\left\Vert x_t - x^{*}  \right\Vert ^{2}
\end{sectionbox}
