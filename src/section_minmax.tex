\section{Min-Max Optimization}

\begin{sectionbox}[Saddle and global minimax point]
    $\left( x^{*} , y^{*}  \right) \in \mathcal{X}  \times \mathcal{Y} $ is a \textbf{saddle point}  of $\phi \left( x, y \right)$ if for any $\left( x, y \right) \in \mathcal{X}  \times \mathcal{Y} $ we have
       $ \phi \left( x^{*} , y \right) &\le \phi \left( x^{*} , y^{*}  \right) &\le \phi \left( x, y^{*}  \right).$

        $\left( x^{*} , y^{*}  \right) \in \mathcal{X} \times \mathcal{Y} $ is a \textbf{global minimax} point of $\phi \left( x, y \right)$ if for any $\left( x, y \right) \in \mathcal{X}  \times \mathcal{Y} $ we have
      $  \phi \left( x^{*} , y \right) &\le \phi \left( x^{*} , y^{*}  \right) &\le \max _{y \in \mathcal{Y} }\phi \left( x, y' \right).$

    Saddle points don't always exist. Global minimax points exist under mild regularity conditions (e.g. if $\phi \left( x, y \right)$ is continuous and $\mathcal{X} $, $\mathcal{Y} $ are compact sets).
\end{sectionbox}


\paragraph{Primal and dual in minimax}
\[\begin{aligned}
    \text{Opt} \left( P \right) &= \min _{x \in \mathcal{X} } \overline{\phi }\left( x \right) = \min _{x \in \mathcal{X} } \max _{y \in \mathcal{Y} } \phi \left( x, y \right)\\
    \text{Opt} \left( D \right) &= \max _{y \in \mathcal{Y} } \underline{\phi }\left( y \right)\min _{x \in \mathcal{X} } \phi \left( x, y \right)
\end{aligned}\]

By weak duality, $\text{Opt} \left( D \right) \le \text{Opt} \left( P \right)$. If a saddle point exists, then strong duality holds $\text{Opt} \left( D \right) = \text{Opt} \left( P \right)$.

\begin{sectionbox}[Von Neumann's Minimax Theorem]
    For any payoff matrix $A \in \mathbb{R} ^{m \times n}$,

    $    \min _{x \in \Delta _m, y \in \Delta _n} x^{T}Ay = \max _{y \in \Delta _n, x \in \Delta _m}x^{T}Ay$

   where $\Delta _m = \left\{ x \in \mathbb{R} ^{m}_+ : \sum_{i = 1}^{m} x_i = 1 \right\}$, $\Delta _n = \left\{ y \in \mathbb{R} ^{n}_+ : \sum_{j = 1}^{n}  y_j = 1 \right\}$.
\end{sectionbox}


\begin{sectionbox}[Sion-Kakutani Minimax theorem]
    Let sets $\mathcal{X}  \subset \mathbb{R} ^{m}$ and $\mathcal{Y}  \subset \mathbb{R} ^{n}$ be two convex compact sets. Let function $\phi \left( x, y \right) : \mathcal{X}  \times \mathcal{Y}  \to \mathbb{R} $ be a continuous function such that for any fixed $y \in \mathcal{Y} $ it is convex in $x$, and for any fixed $x \in \mathcal{X} $ it is concave in $y$. Then $\phi \left( \mathcal{X} , \mathcal{Y}  \right)$ has a saddle point on $\mathcal{X}  \times \mathcal{Y} $ and
    \[\begin{aligned}
        \max _{y \in \mathcal{Y} }\min _{x \in \mathcal{X} }\phi \left( x, y \right) = \min _{x \in \mathcal{X} }\max _{y \in \mathcal{Y} }\phi \left( x, y \right)
    \end{aligned}\]
\end{sectionbox}



% \begin{color}{violet}
%     Skip things...
% \end{color}

\begin{sectionbox}[Smoothness in min-max]
    We say $\phi \left( x, y \right)$ is $L$-Lipschitz smooth jointly in $x$ and $y$ if for any $x_1, x_2 \in \mathcal{X} $ and $y_1, y_2 \in \mathcal{Y} $
    \[\begin{aligned}
        \left\Vert \nabla _x \phi \left( x_1, y_1 \right) - \nabla _x \phi \left( x_2, y_t \right) \right\Vert  &\le  L\left( \left\Vert x_1 - x_2 \right\Vert  + \left\Vert y_1 - y_2 \right\Vert  \right)\\
        \left\Vert \nabla _y \phi \left( x_1, y_1 \right) - \nabla _y \phi \left( x_2, y_2 \right) \right\Vert  &\le L \left( \left\Vert x_1 - x_2 \right\Vert + \left\Vert y_1 - y_2 \right\Vert  \right)
    \end{aligned}\]
\end{sectionbox}

\begin{sectionbox}[Gradient Descent Ascent (GDA)]
    Gradient Descent Ascent has update rules

 $       x_{t+1} &= \Pi _\mathcal{X} \left( x_t - \eta \nabla _x \phi \left( x_t, y_t \right) \right)$

  $      y_{t+1} &= \Pi _\mathcal{Y}  \left( y_t + \eta \nabla _y \phi \left( x_t, y_t \right) \right)$

    In the strongly-convex strongly-concave and smooth setting, GDA converges linearly if using sufficiently small stepsize.

    In a convex-concave setting, GDA with constant stepsize may not converge.
\end{sectionbox}



\begin{sectionbox}[Extragradient Method (EG)]
    Extragradient Method uses the update rules

 $       x_{t+\frac{1}{2} } &= \Pi _\mathcal{X}  \left( x_t - \eta \nabla _x \phi \left( x_t, y_t \right) \right), y_{t+\frac{1}{2} } = \Pi _{\mathcal{Y} }\left( y_t + \eta \nabla _y \phi \left( x_t, y_t \right) \right)$

  $      x_{t+1} &= \Pi _\mathcal{X}  \left( x_t - \eta \nabla _x \phi \left( x_{t+\frac{1}{2}} , y_{t+\frac{1}{2}}\right) \right), y_{t+1} = \Pi _\mathcal{Y}  \left( y_t + \eta \nabla _y \phi \left( x_{t+\frac{1}{2}}, y_{t+\frac{1}{2}} \right) \right).$

    It is guaranteed to converge to a saddle point in the convex-concave setting.

\end{sectionbox}

\begin{sectionbox}[Convergence of EG]
    Assume $D_\mathcal{X} $ and $D_\mathcal{Y} $ are diameters of $\mathcal{X} $ and $\mathcal{Y} $ respectively. Under smoothness and the convex-concave setting, EG with stepsize $\eta \le \frac{1}{2L}$ satisfies
      $  \max _{y \in \mathcal{Y} }\phi \left( \frac{1}{T} \sum_{t=  1}^{T}  x_{t+\frac{1}{2}}, y \right) - \min _{x \in \mathcal{X} }\phi \left( x, \frac{1}{T} \sum_{t = 1}^{T}  y_{t+\frac{1}{2}} \right) \le \frac{D^{2}_\mathcal{X}  + D^{2}_{\mathcal{Y} }}{2\eta T}.$

\end{sectionbox}


% \begin{color}{violet}
%     Skip variational inequalities $:\sim $
% \end{color}
